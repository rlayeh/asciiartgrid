const AdUrlCreator = require('../api/api').AdUrlCreator;

class AdsStore{
	constructor(){
		this._alreadyDisplayed = [];
	}

	get(index){
		if(this._alreadyDisplayed[index]){
			return this._alreadyDisplayed[index];
		}

		const newUrl = this._getNewUrl();

		this._alreadyDisplayed[index] = newUrl;
		return newUrl;
	}

	clear(){
		this._alreadyDisplayed = [];
		return this;
	}

	_getNewUrl(){
		let newAdUrl = this._createUrl();

		while(this._alreadyExists(newAdUrl)){
			newAdUrl = this._createUrl();
		}

		return newAdUrl;
	}

	_createUrl(){
		const newRandom = this._getNewRandomNumber();

		return AdUrlCreator.get(newRandom);
	}

	_getNewRandomNumber(){
		return Math.floor((Math.random() * 1000000) + 1);
	}

	_alreadyExists(newAdUrl){
		return this._alreadyDisplayed.some((adUrl)=>{
			return adUrl === newAdUrl;
		});
	}
}

module.exports = AdsStore;