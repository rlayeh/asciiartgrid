const DataRetriever = require('../api/api').DataRetriever;
const Page = require('./Page');
const Entity = require('./Entity');

class DataStore{
	constructor(entityName){
		this._entityName = entityName;
		this._pages = [];
		this._pageSize = 20;
		this._onStoreChanged = null;
		this._sort = null;
		this._pendingRequests = [];
	}

	pageSize(size){
		this._pageSize = size;
		return this;
	}

	sort(fieldName){
		this._sort = fieldName;
		this._clearStore();
		return this;
	}

	onStoreChanged(storeChanged){
		this._onStoreChanged = storeChanged;
		return this;
	}

	getPage(index){
		if(this._pages.length > index){
			if(this._pages.length -1 === index){
				this._preFetchNext(index);
			}

			return this._pages[index];
		}

		this._fetchPage(index);

		return null;
	}

	_convertToEntity(entityAsObject){
		return new Entity(entityAsObject);
	}

	_convertEntities(entities){
		return entities.map((entity)=>{
			return this._convertToEntity(entity);
		});
	}

	_fetchPage(page){
		if(this._requestPending(page)){
			return;
		}

		this._pendingRequests.push(page);

		const startIndex = page * this._pageSize;

		const retriever = new DataRetriever(this._entityName)
				.limit(this._pageSize)
				.skip(startIndex);

		if(this._sort){
			retriever.sort(this._sort);
		}
		
		return retriever.get()
						.then(this._pageFetched.bind(this, page))
						.catch(this._handleException.bind(this, page));	
	}

	_pageFetched(pageIndex, entities){
		if(entities){
			const isLastPage = entities.length < this._pageSize;
			const convertedEntities = this._convertEntities(entities);

			this._pages[pageIndex] = new Page(convertedEntities, isLastPage);
		}
		else if(this._pages.length){
			this._pages[this._pages.length - 1].lastPage(true);
		}

		this._removeFromPending(pageIndex);
		this._fireOnStoreChanged();
	}

	_preFetchNext(pageIndex){
		this._fetchPage(pageIndex+1);
	}

	_fireOnStoreChanged(){
		if(this._onStoreChanged){
			this._onStoreChanged();
		}
	}

	_handleException(pageIndex){
		this._removeFromPending(pageIndex);
		throw 'Error while fetching data from the server';
	}

	_clearStore(){
		this._pages = [];
		this._fireOnStoreChanged();
	}

	_requestPending(pageIndex){
		return this._pendingRequests.indexOf(pageIndex) > -1;
	}

	_removeFromPending(pageIndex){
		const index = this._pendingRequests.indexOf(pageIndex);
		if(index>-1){
			this._pendingRequests.splice(index, 1);
		}
	}
}

module.exports = DataStore;