class Entity{
	constructor(entityAsObject){
		this._buildEntity(entityAsObject);
	}

	getValue(field){
		return this[this._getFieldName(field)];
	}

	getFormattedValue(field){
		const fieldName = this._getFieldName(field);

		const methodName = this._getFormattedMethodName(field);
		if(this[methodName]){
			return this[methodName]();
		}

		return this[fieldName];
	}

	_getFieldName(field){
		return '_' + field;
	}

	_buildEntity(entityAsObject){
		for(let propertyName in entityAsObject){
			this[this._getFieldName(propertyName)] = entityAsObject[propertyName];
		}
	}

	_getFormattedMethodName(field){
		let formattedFieldName = field.charAt(0).toUpperCase() + field.slice(1);
		return 'getFormatted' + formattedFieldName;
	}
}

module.exports = Entity;