class Page{
	constructor(entities, isLast){
		this._entities = entities;
		this._lastPage = isLast;
	}

	lastPage(isLast){
		this._lastPage = isLast;
		return this;
	}

	get(index){
		if(this._entities.length > index){
			return this._entities[index];
		}

		throw 'index out of range';
	}

	map(func){
		return this._entities.map(func);
	}

	isLastPage(){
		return this._lastPage;
	}

}

module.exports = Page;