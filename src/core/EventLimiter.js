class EventLimiter{
    constructor(limit, callback) {
    	this._callback = callback;
    	this._limit = limit;
        this._storedArguments = null;
        this._timeoutId = null;
    };

    invoke(){
   		this._storedArguments = arguments;

         if(this._hasOutstandingInvocation()){
            return;
        }

        this._timeoutId = setTimeout(this._executeInvoke.bind(this), this._limit);
    }

    _hasOutstandingInvocation(){
    	return this._timeoutId !== null;
    }
     
    _executeInvoke(){
    	this._timeoutId = null;
    	this._callback.apply(null, this._storedArguments);
    }
}

module.exports = EventLimiter;