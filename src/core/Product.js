const Entity = require('./Entity');

const WEEK_IN_MILIES = 604800000;
const DAY_IM_MILIES = 86400000;
const HOUR_IM_MILIES = 3600000;

class Product extends Entity{
	constructor(entity){
		super(entity);	
	}

	getFormattedPrice(){
		return '$' + this.getValue('price')/100
	}

	getFormattedDate(){
		const dateAsString = this.getValue('date');
		const relativeDateMilies = this._getRelativeDate(dateAsString);
		if(this._dateOlderThanAWeek(relativeDateMilies)){
			return dateAsString;
		}
		return this._getRelativeDateDescription(relativeDateMilies);
	}

	_getRelativeDateDescription(relativeDateMilies){
		const days = Math.floor(relativeDateMilies / DAY_IM_MILIES);
		const hours = Math.floor(relativeDateMilies % DAY_IM_MILIES / HOUR_IM_MILIES);

		return days + ' days and ' + hours + ' hours ago';
	}

	_dateOlderThanAWeek(milies){
		return milies > WEEK_IN_MILIES;
	}

	_getRelativeDate(dateAsString){
		const tempDate = new Date(dateAsString);
		return new Date() - tempDate;
	}

}

module.exports = Product;