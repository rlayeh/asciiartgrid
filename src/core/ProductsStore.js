const DataStore = require('./DataStore');
const Product = require('./Product');

class ProductStore extends DataStore{
	constructor(){
		super('products');
	}

	_convertToEntity(entityAsObject){
		return new Product(entityAsObject);
	}
}

module.exports = ProductStore;