const ReactDom = require('react-dom');
const React = require('react');
const core = require('./core/core');
const Grid = require('./ui/ui').Grid;

class Main{
	constructor(){
		this._dataStore = new core.ProductsStore()
								.onStoreChanged(this._storeChanged.bind(this));
		this._adsStore = new core.AdsStore();
		this._gridContainer = document.getElementById('productsContainer');
	}

	render(){
		const grid = <Grid pageGetter={this._pageGetter.bind(this)}
						   adsGetter={this._adGetter.bind(this)}
						   onSort={this._onSort.bind(this)}
						   columnNames={this._getColumnNames()}
						   sortableColumns={this._getSortableColumnNames()} />

		ReactDom.render(grid, this._gridContainer);
	}

	_getColumnNames(){
		return ['face', 'date', 'size', 'price', 'id'];

	}

	_getSortableColumnNames(){
		return ['size', 'price', 'id'];
	}

	_pageGetter(index){
		return this._dataStore.getPage(index);
	}

	_adGetter(index){
		return this._adsStore.get(index);
	}

	_storeChanged(){
		this.render();
	}

	_onSort(columnName){
		this._adsStore.clear();
		this._dataStore.sort(columnName);
	}
}

document.addEventListener('DOMContentLoaded', ()=>{
	new Main().render();
}, false);
