const HttpRequest = require('../net/net').HttpRequest;

class DataRetriever{
	constructor(entityName){
		this._entityName = entityName;
		this._sort = null;
		this._limit = 0;
		this._skip = 0;
	}

	limit(limitSize){
		this._limit = limitSize;
		return this;
	}

	skip(skipSize){
		this._skip = skipSize;
		return this;
	}

	sort(name){
		this._sort = name;
	}

	get(){
		const request = this._getRequest();
		this._extendwithAnAttribute(request, 'skip');
		this._extendwithAnAttribute(request, 'limit');
		this._extendwithAnAttribute(request, 'sort');

		return request.execute().catch(this._retrievingDataFailed);
	}

	_getRequest(){
		return new HttpRequest('/api')
			.appendUrl(this._entityName)
			.async(true)
			.method('GET')
			.responseAsObject(true)
			.splitByLines(true);
	}

	_extendwithAnAttribute(request, attrName){
		const attr = this['_' + attrName];
		if(attr){
			request.appendAttribute(attrName, attr);
		}
	}

	_retrievingDataFailed(){
		throw 'error while fetching data';
	}

};

module.exports = DataRetriever;