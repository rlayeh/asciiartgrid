const UrlCreator = require('../net/net').UrlCreator;

class AdUrlCreator{
	static get(adNumber){
		return new UrlCreator('/ad')
					.appendAttribute('r', adNumber)
					.create();
	}
};

module.exports = AdUrlCreator;