class UrlCreator{
	constructor(basePath){
		this._pathParts = basePath ? [basePath] : [];
		this._separator = '/';
		this._pathAttributes = [];
	}

	append(pathPart){
		this._pathParts.push(pathPart);
		return this;
	}

	appendAttribute(attributeName, value){
		this._pathAttributes.push(attributeName + '=' + value);
		return this;
	}

	separator(separator){
		this._separator = separator;
		return this;
	}

	create(){
		let url = this._pathParts.join(this._separator);

		if(this._pathAttributes.length){
			url += "?" + this._pathAttributes.join('&');
		}

		return url
	}
};

module.exports = UrlCreator;