const UrlCreator = require('./UrlCreator');
const nanoajax = require('nanoajax');


class HttpRequest{
	constructor(path){
		this._urlCreator = new UrlCreator(path);
		this._method = 'GET';
		this._responseAsObject = false;
		this._async = false;
		this._splitByLines = false;
	}

	async(async){
		this._async = async;
		return this;
	}

	appendUrl(pathPart){
		this._urlCreator.append(pathPart);
		return this;
	}

	appendAttribute(attributeName, value){
		this._urlCreator.appendAttribute(attributeName, value);
		return this;
	}

	splitByLines(splitByLines){
		this._splitByLines = splitByLines;
		return this;
	}

	method(method){
		this._method = method;
		return this;
	}


	responseAsObject(responseAsObject){
		this._responseAsObject = responseAsObject;
		return this;
	}

	execute(){
		const config = this._getConfig();
		return new Promise((resolve, reject)=>{
			this._executeAjax(config, resolve, reject);
		});
	}

	_getConfig(){
		const path = this._urlCreator.create();
		if(!path){
			throw "Path is not specified";
 		}

 		return {
 			url: path,
 			method: this._method
 		};
	}

	_executeAjax(config, onSuccess, onFailed){
		const responseHandler = (code, responseText, request) => {
			this._response(code, responseText, request, onSuccess, onFailed);
		};

		if(!this._async){
			nanoajax.ajax(config, responseHandler);
			return;
		}
		
		//trigger in async mode
		this._executeAsync(config, responseHandler);
	}

	_executeAsync(config, handler){
		setTimeout(() => nanoajax.ajax(config, handler), 0);
	}

	_response(code, responseText, request, onSuccess, onFailed){
		if(this._isSuccesfull(code)){
			onSuccess(this._getConvertedResponse(responseText));
			return;
		}
		
		onFailed(code, responseText, request);
	}

	_getConvertedResponse(responseText){
		return this._responseAsObject && responseText ? this._getResponseAsObject(responseText) : responseText;
	}

	_getResponseAsObject(responseText){
		if(this.splitByLines){
			const lines = this._getSplittedResponse(responseText);

			return lines.map((line)=>JSON.parse(line));
		}

		return JSON.parse(responseText);
	}

	_getSplittedResponse(responseText){
		const splitted = responseText.split('\n');

		return splitted.filter((line)=>line);
	}

	_isSuccesfull(code){
		return code >= 200 && code < 300;
	}

	_fireOnSuccess(response){
		if(this._onSuccess){
			this._onSuccess(response);
		}
	}

	_fireOnFailed(code, responseText, request){
		if(this._onFailed){
			this._onFailed(code, responseText, request);
		}
	}

}

module.exports = HttpRequest;