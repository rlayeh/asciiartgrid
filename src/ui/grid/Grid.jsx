const React = require('react');
const HeaderRow = require('./HeaderRow.jsx');
const GridPage = require('./GridPage.jsx');
const GridMaskPanel = require('./GridMaskPanel.jsx');
const EventLimiter = require('../../core/core').EventLimiter;

const Grid = React.createClass({
	propTypes: {
	    pageGetter  : React.PropTypes.func.isRequired,
	    adsGetter   : React.PropTypes.func.isRequired,
	    onSort	    : React.PropTypes.func.isRequired,
	    columnNames: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
	    sortableColumns: React.PropTypes.arrayOf(React.PropTypes.string)

    },
    getInitialState:function(){
    	return {
    		currentLastPageIndex: 0,
            eventLimiter: new EventLimiter(200, this._scrollEnded)
    	}
    },
    render: function(){
    	const pages = this._getPagesToRender();
    	const isLoading = this._isLoading(pages);
    	const isFinished = isLoading ? false : this._isFinished(pages);

    	return <div className='grid'>
    				{this._renderMaskPanel(isLoading)}
    				<div className='grid-header-container'>	
    					{this._renderHeader()}
    				</div>
    				<div className='grid-rows-container' onScroll={this._scroll}>
    					{this._renderPages(pages)}
    					{this._renderFooter(isFinished)}
    				</div>
    			</div>
    },

    _renderMaskPanel: function(visible){
    	return <GridMaskPanel visible={visible} />;
    },

    _scroll: function(ev){
        const target = ev.target;

        if((target.scrollTop + target.clientHeight) >= (target.scrollHeight - 50)){
           this.state.eventLimiter.invoke();
        }
    },

    _scrollEnded: function(){
        this._loadMore();
    },

    _renderHeader: function(){
    	return <HeaderRow columnNames={this.props.columnNames}
    					  sortableColumns={this.props.sortableColumns}
    					  onClick={this._headerClicked} />
    },

    _headerClicked: function(columnName){
    	this.props.onSort(columnName);

    	this.setState({
    		currentLastPageIndex: 0
    	});
    },

    _renderPages: function(pages){

    	let counter = 0;
    	return pages.map((page)=>{
            let ad = this._getAd(counter);
            counter++;
            
            if(!page){
                return <div key={'gridPage' + counter} />; //dummy div as a placeholder before it will be fetched
            }

    		return <GridPage key={'gridPage' + counter} page={page} adUrl={ad} columnNames={this.props.columnNames} />
    	});
    },

    _renderFooter: function(isFinished){
    	if(isFinished){
    		return <div className={'grid-finished'}>~ end of catalogue ~</div>;
    	}

    	return null;
    },

    _loadMore: function(){
        const lastPage = this._getPage(this.state.currentLastPageIndex);
        if(lastPage && lastPage.isLastPage()){
            return;
        }
        this.setState({
            currentLastPageIndex: this.state.currentLastPageIndex + 1
        });
    },

    _getPagesToRender: function(){
    	const pages = [];
    	for(let i=0;i<this.state.currentLastPageIndex+1; i++){
    		pages.push(this._getPage(i));
    	}

    	return pages;
    },

    _isLoading: function(pages){
    	return pages.some((page)=>{
    		return !page;
    	});
    },

    _isFinished: function(pages){
    	return pages.some((page)=>{
    		return page.isLastPage();
    	});
    },

    _getPage: function(index){
    	return this.props.pageGetter(index);
    },

    _getAd: function(index){
    	return this.props.adsGetter(index);
    }

});


module.exports = Grid;