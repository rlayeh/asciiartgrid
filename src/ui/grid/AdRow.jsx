const React = require('react');
const ReactAddOns = require('react-addons');

const AdRow = React.createClass({
	propTypes: {
	    url		: React.PropTypes.string.isRequired
    },
	mixins: [ReactAddOns.PureRenderMixin],
	render: function(){
		return <div className={'ad-row'}>
					<img src={this.props.url} />
				</div>;
	}
});

module.exports = AdRow;
