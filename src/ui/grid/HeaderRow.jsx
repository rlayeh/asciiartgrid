const React = require('react');
const ReactAddOns = require('react-addons');
const Row = require('./Row.jsx');

const HeaderRow = React.createClass({
	propTypes: {
	    columnNames: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
	    sortableColumns: React.PropTypes.arrayOf(React.PropTypes.string),
	    onClick   : React.PropTypes.func

    },
	mixins: [ReactAddOns.PureRenderMixin],
	getDefaultProps: function(){
		return {
			sortableColumns: []
		};
	},
	render: function(){
		return <Row className={'grid-header'} 
					cellValues={this.props.columnNames} 
					isClickable={this._isCellClickable} 
					onClick={this._onCellClick} />
	},

	_isCellClickable: function(cellValue){
		return this.props.sortableColumns.some((column)=>{
			return column.toLowerCase() === cellValue.toLowerCase();
		});
	},

	_onCellClick: function(columnName){
		if(this.props.onClick){
			this.props.onClick(columnName.toLowerCase());
		}
	}

});

module.exports = HeaderRow;