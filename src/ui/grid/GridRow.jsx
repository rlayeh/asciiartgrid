const React = require('react');
const ReactAddOns = require('react-addons');
const Row = require('./Row.jsx');
const Entity = require('../../core/core').Entity;

const GridRow = React.createClass({
	propTypes: {
	    entity 	  : React.PropTypes.instanceOf(Entity).isRequired,
	    columnNames: React.PropTypes.arrayOf(React.PropTypes.string).isRequired
    },
	mixins: [ReactAddOns.PureRenderMixin],
	render: function(){
		return <Row className={'grid-row'} 
					style={this._getRowStyle()}
					cellValues={this._getCellValues()} />
	},

	_getCellValues: function(){
		return this.props.columnNames.map((columnName)=>{
			return this.props.entity.getFormattedValue(columnName);
		});
	},

	_getRowSize: function(){
		return this.props.entity.getValue('size');
	},

	_getRowStyle: function(){
		const style = {};
		const size = this._getRowSize();
		if(size){
			style.minHeight = size;
		}

		return style;
	}
});

module.exports = GridRow;