const React = require('react');
const ReactAddOns = require('react-addons');
const GridRow = require('./GridRow.jsx');
const AdRow = require('./AdRow.jsx');
const Page = require('../../core/core.js').Page;

const GridPage = React.createClass({
	propTypes: {
	    page   	   : React.PropTypes.instanceOf(Page).isRequired,
	    adUrl	   : React.PropTypes.string.isRequired,
	    columnNames: React.PropTypes.arrayOf(React.PropTypes.string).isRequired
    },
	shouldComponentUpdate: function(){
		return false;//this component don't have to be updated. It will be removed and replaced if change is required
	},
	render: function(){
		const rows = this._getRows();
		rows.push(this._getAd());

		return <div className="grid-page">{rows}</div>;
	},

	_getRows: function(){
		let counter = 0;
		return this.props.page.map((entity)=>{
			return <GridRow key={'gridRow' + counter++} entity={entity} columnNames={this.props.columnNames} />;
		});
	},

	_getAd: function(){
		return <AdRow key={'pageAd'} url={this.props.adUrl} />;
	}
});

module.exports = GridPage;