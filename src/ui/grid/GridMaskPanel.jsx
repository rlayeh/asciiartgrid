const React = require('react');
const ReactAddOns = require('react-addons');

const GridMaskPanel = React.createClass({
	propTypes: {
	    visible		: React.PropTypes.bool
    },
	mixins: [ReactAddOns.PureRenderMixin],
	render: function(){
		return <div className={this._getCssClass()} onClick={this.props.onClick} >
					<span>loading...</span>
			   </div>;
	},

	_getCssClass: function(){
		let css = 'grid-mask-panel';

		if(this.props.visible){
			css += ' visible';
		}

		return css;
	}

});

module.exports = GridMaskPanel;