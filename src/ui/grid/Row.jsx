const React = require('react');
const ReactAddOns = require('react-addons');
const GridCell = require('./GridCell.jsx');

const Row = React.createClass({
	propTypes: {
	    cellValues		: React.PropTypes.array.isRequired,
	    isClickable		: React.PropTypes.func,
	    className 		: React.PropTypes.string.isRequired,
	    onClick   		: React.PropTypes.func,
	    style			: React.PropTypes.object

    },
	mixins: [ReactAddOns.PureRenderMixin],
	render: function(){
		const cells = this._getCells();

		return <div style={this.props.style} className={this.props.className}>
					{cells}
				</div>;
	},

	_getCells: function(){
		let counter = 0;
		return this.props.cellValues.map((cellValue)=>{
			let onClick = this._isClickable(cellValue) ? this._cellClicked : null;

			return <GridCell key={counter++} value={cellValue} onClick={onClick} />;
		});
	},

	_isClickable: function(cellValue){
		return this.props.onClick  && this.props.isClickable && this.props.isClickable(cellValue);
	},

	_cellClicked: function(cellName){
		this.props.onClick(cellName);
	}

});

module.exports = Row;