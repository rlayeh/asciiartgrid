const React = require('react');
const ReactAddOns = require('react-addons');

const GridCell = React.createClass({
	propTypes: {
	    value     : React.PropTypes.oneOfType([
						      React.PropTypes.string,
						      React.PropTypes.number]).isRequired,
	    onClick   : React.PropTypes.func
    },
	mixins: [ReactAddOns.PureRenderMixin],
	render: function(){
		return <div className={this._getCssClass()} onClick={this._cellClicked} >
					{this.props.value}
				</div>;
	},

	_cellClicked: function(){
		if(this.props.onClick){
			this.props.onClick(this.props.value);
		}
	},

	_isClickable: function(){
		return !!this.props.onClick;
	},

	_getCssClass: function(){
		let css = 'grid-cell';

		if(this._isClickable()){
			css += ' clickable';
		}
		return css;
	}

});

module.exports = GridCell;